﻿using System;
using System.Text.RegularExpressions;
using System.IO;

namespace MTGA_Feeler
{
    public static class MTGAVault
    {
        private static string LogLocation => $@"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}\AppData\LocalLow\Wizards Of The Coast\MTGA\output_log.txt";

        public static double VaultPercentage
        {
            get
            {
                try
                {
                    string text = File.ReadAllText(LogLocation);
                    Regex regex = new Regex("(\"vaultProgress\": )([0-9.]+)", RegexOptions.RightToLeft);
                    return Convert.ToDouble(regex.Match(text).Groups[2].Value);
                }
                catch
                {
                    return -1;
                }
                
            }
        }
    }
}
